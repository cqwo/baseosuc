package com.cqwo.xxx2.plugin.oauth.miniapp.framework;

import com.cqwo.xxx2.core.exception.CWMException;
import com.cqwo.xxx2.web.framework.controller.BaseApiController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller(value = "OauthMiniAppApiController")
public class OauthMiniAppApiController extends BaseApiController {

    @Override
    @ModelAttribute
    public void setInitialize(HttpServletResponse response) throws IOException, CWMException {

        super.setInitialize(response);


    }

}
