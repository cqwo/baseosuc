/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆青沃科技有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.cqwo.xxx2.plugin.oauth.miniapp.model;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import com.cqwo.ucenter.client.domain.PartUserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorOnLoginModel {

    private String sessionId = "";

    /**
     * 用户OpenId
     **/
    private String openId = "";

    /**
     * unionId
     */
    private String unionId = "";

    /**
     * SessionKey
     **/
    private String sessionKey = "";

    /**
     * 用户Uid
     **/
    private String uid = "";

    /**
     * Token
     **/
    private String token = "";

    /**
     * 用户基本信息
     **/
    private PartUserInfo userInfo;

}
