/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.xxx2.plugin.oauth.miniapp.model;


import com.cqwo.ucenter.client.domain.PartUserInfo;
import com.cqwo.ucenter.client.domain.UserRankInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by cqnews on 2017/12/25.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginMessageModel {

    /**
     * 用户登录成功Uid
     */
    private String uid = "";


    /**
     * 用户登录token
     */
    private String token = "";

    /**
     * 唯一邀请码
     */
    private String invitCode = "";


    /**
     * 用户模型
     */
    private PartUserInfo partUserInfo;

    /**
     * 用户分组
     */
    private UserRankInfo userRankInfo;


}
