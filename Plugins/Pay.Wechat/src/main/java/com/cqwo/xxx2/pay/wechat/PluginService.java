package com.cqwo.xxx2.pay.wechat;

import com.cqwo.ucenter.client.domain.PartUserInfo;
import com.cqwo.xxx2.core.plugin.interface2.IPayPlugin;
import com.cqwo.xxx2.core.log.Logs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;


@Component(value = "PayWechatPluginService")
public class PluginService implements IPayPlugin {

    @Autowired
    private Logs logs;

    @Override
    public String getPayUrl() {
        return "/wechat/pay";
    }

    @Override
    public Integer getPayMode() {
        return 0;
    }

    @Override
    public double getPayFee(double recordAmount, Timestamp buyTime, PartUserInfo partUserInfo) {
        return 0;
    }

    @Override
    public String getConfigUrl() {
        return "/admin/wehcat/payconfig";
    }


    @PostConstruct
    @Override
    public void initPlugin() {
        logs.write("初始化插件");
        System.out.println("初始化插件");
    }

}
