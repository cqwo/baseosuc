package com.cqwo.xxx2.core.model;

import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 分页模型
 *
 * @author cqnews
 * @param <T>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageInfo<T> implements Serializable {



    /**
     * 分页模型
     */
    private PageModel pageModel;

    /**
     * 列表
     */
    private List<T> list;
}
