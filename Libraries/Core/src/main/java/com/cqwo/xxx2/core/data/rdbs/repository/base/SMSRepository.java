package com.cqwo.xxx2.core.data.rdbs.repository.base;

import com.cqwo.xxx2.core.data.rdbs.repository.BaseRepository;
import com.cqwo.xxx2.core.domain.sms.SMSInfo;

public interface SMSRepository extends BaseRepository<SMSInfo, Integer> {
}
