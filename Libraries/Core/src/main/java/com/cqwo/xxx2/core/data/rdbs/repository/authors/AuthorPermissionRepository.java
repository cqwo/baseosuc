
package com.cqwo.xxx2.core.data.rdbs.repository.authors;

import com.cqwo.xxx2.core.data.rdbs.repository.BaseRepository;
import com.cqwo.xxx2.core.domain.authors.AuthorPermissionInfo;

public interface AuthorPermissionRepository extends BaseRepository<AuthorPermissionInfo, Integer> {
}