/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.xxx2.core.domain.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 在线时间统计
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_user_onlinetime")
public class OnlineTimeInfo implements Serializable {

    private static final long serialVersionUID = 8712096278634674842L;
    /**
     * 在线时间统计
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ColumnDefault(value = "0")
    private Integer id = 0;

    /**
     * Uid
     **/
    @Column(name = "uid")
    @ColumnDefault(value = "''")
    private String uid = "";

    /**
     * 总计在线
     **/
    @Column(name = "total")
    @ColumnDefault(value = "0")
    private Integer total = 0;

    /**
     * 年
     **/
    @Column(name = "year")
    @ColumnDefault(value = "0")
    private Integer year = 0;

    /**
     * 月
     **/
    @Column(name = "month")
    @ColumnDefault(value = "0")
    private Integer month = 0;

    /**
     * 周
     **/
    @Column(name = "week")
    @ColumnDefault(value = "0")
    private Integer week = 0;

    /**
     * 日
     **/
    @Column(name = "day")
    @ColumnDefault(value = "0")
    private Integer day = 0;
    
    /**
     * 更新时间
     **/
    @Column(name = "updatetime")
    @ColumnDefault(value = "0")
    private Integer updateTime = 0;

}