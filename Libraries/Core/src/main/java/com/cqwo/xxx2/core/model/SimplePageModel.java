package com.cqwo.xxx2.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimplePageModel implements Serializable {

    /**
     * 每页条数
     */
    private Integer pageSize = 10;


    /**
     * 当前页数
     */
    private Integer pageNumber = 1;


    /**
     * 总页数
     */
    private Integer totalPages = 0;

    public SimplePageModel(Integer pageSize, int pageNumber, int totalPages) {

        if (pageSize > 0) {
            this.pageSize = pageSize;
        } else {
            this.pageSize = 10;
        }

        if (pageNumber > 0) {
            this.pageNumber = pageNumber;
        } else {
            this.pageNumber = 1;
        }

        this.totalPages = totalPages;

    }


}
