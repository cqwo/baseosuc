/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.xxx2.core.log;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

/**
 * Created by cqnews on 2017/4/15.
 */
@Service(value = "CWMLog")
@Getter
public class CWMLogs {

    @Autowired(required = false)
    private ILogStrategy iLogStrategy;

    /**
     * 写入日志
     *
     * @param message 消息
     */
    public void write(String message) {
        iLogStrategy.write(message);
    }

    /// <summary>
    /// 写入日志
    /// </summary>
    /// <param name="ex">异常对象</param>

    /**
     * 写入日志
     *
     * @param ex 异常对象
     */
    public void write(Exception ex) {
        try {
            iLogStrategy.write(MessageFormat.format("方法:{0},异常信息:{1}", ex.getStackTrace(), ex.getMessage()));
        } catch (Exception ex2) {
            ex.printStackTrace();
            ex2.printStackTrace();
        }
    }

    /**
     * 写入日志
     *
     * @param ex      异常
     * @param message 消息
     */
    public void write(Exception ex, String message) {

        try {
            iLogStrategy.write(MessageFormat.format("异常描述:{0},方法:{1},异常信息:{2}", message, ex.getStackTrace(), ex.getMessage()));
        } catch (Exception ex2) {
            ex.printStackTrace();
            ex2.printStackTrace();
        }
    }


    /**
     * debug输出
     *
     * @param message 消息
     */
    public void debug(String message) {
        try {
            iLogStrategy.debug(message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * info输出
     *
     * @param message 消息
     */
    public void info(String message) {
        try {
            iLogStrategy.info(message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * warn输出
     *
     * @param message 消息
     */
    public void warn(String message) {
        try {
            iLogStrategy.warn(message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * error输出
     *
     * @param message 消息
     */
    public void error(String message) {
        try {
            iLogStrategy.error(message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * fatal输出
     *
     * @param message 消息
     */
    public void fatal(String message) {
        try {
            iLogStrategy.fatal(message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
