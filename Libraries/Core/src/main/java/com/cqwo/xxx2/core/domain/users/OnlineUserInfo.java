/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.xxx2.core.domain.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

//在线用户
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_user_onlineusers")
public class OnlineUserInfo implements Serializable {

    private static final long serialVersionUID = 512440548844532098L;
    /**
     * 在线用户编号
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "olid")
    private Integer olId = 0;
    /**
     * 在线用户id
     **/
    @Column(name = "uid", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    private String uid = "";
    /**
     * 在线用户sid
     **/
    @Column(name = "sid", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String sid = "";
    /**
     * 用户昵称
     **/
    @Column(name = "nickname", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String nickName = "";
    /**
     * 在线用户ip
     **/
    @Column(name = "ip", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String ip = "";

    /**
     * 注册区域
     **/
    @Column(name = "regionid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer regionId = 0;
    /**
     * 更新时间
     **/
    @Column(name = "updatetime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer updateTime = 0;


}