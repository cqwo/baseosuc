
package com.cqwo.xxx2.core.data.rdbs.repository.base;

import com.cqwo.xxx2.core.data.rdbs.repository.BaseRepository;
import com.cqwo.xxx2.core.domain.base.AttachmentInfo;

public interface AttachmentRepository extends BaseRepository<AttachmentInfo, Integer> {


}