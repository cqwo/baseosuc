/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.xxx2.core.domain.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

//用户日志
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_user_creditlogs")
public class CreditLogInfo implements Serializable {


    private static final long serialVersionUID = 5540483259055734690L;
    /**
     * 日志Id
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "logid")
    private Integer logId = 0;

    /**
     * Uid
     **/
    @Column(name = "uid", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    private String uid = "";

    /**
     * 支付积分
     **/
    @Column(name = "paycredits", nullable = false)
    @ColumnDefault(value = "0")
    private Integer payCredits = 0;

    /**
     * 积分等级
     **/
    @Column(name = "rankcredits", nullable = false)
    @ColumnDefault(value = "0")
    private Integer rankCredits = 0;

    /**
     * 动作类型
     **/
    @Column(name = "action", nullable = false)
    @ColumnDefault(value = "0")
    private Integer action = 0;

    /**
     * 动作代码
     **/
    @Column(name = "actioncode", nullable = false)
    @ColumnDefault(value = "0")
    private Integer actionCode = 0;

    /**
     * 动作时间
     **/
    @Column(name = "actiontime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer actionTime = 0;

    /**
     * 动作描述
     **/
    @Column(name = "actiondes", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String actionDes = "";

    /**
     * 操作人
     **/
    @Column(name = "operator", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    private String operator = "";

}