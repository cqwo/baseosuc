package com.cqwo.xxx2.core.data.rdbs.repository.base;

import com.cqwo.xxx2.core.data.rdbs.repository.BaseRepository;
import com.cqwo.xxx2.core.domain.base.RegionInfo;

public interface RegionRepository extends BaseRepository<RegionInfo, Integer> {

    Integer countByRegionId(Integer regionId);

}
