/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.xxx2.core.exception;

import java.io.Serializable;

/**
 * Created by cqnews on 2017/3/20.
 */
public class CWMAPIException extends CWMException implements Serializable {

    private static final long serialVersionUID = -8042053209328899711L;

    public CWMAPIException() {
    }

    public CWMAPIException(String message) {
        super(message);
    }

    public CWMAPIException(String message, Throwable cause) {
        super(message, cause);
    }

    public CWMAPIException(Throwable cause) {
        super(cause);
    }

    public CWMAPIException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
