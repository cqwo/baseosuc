package com.cqwo.xxx2.core.data.rdbs.repository.base;

import com.cqwo.xxx2.core.data.rdbs.repository.BaseRepository;
import com.cqwo.xxx2.core.domain.base.BannedIPInfo;

public interface BannedIPRepository extends BaseRepository<BannedIPInfo, Integer> {
}
