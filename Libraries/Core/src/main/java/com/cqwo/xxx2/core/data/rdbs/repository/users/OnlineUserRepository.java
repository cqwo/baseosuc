package com.cqwo.xxx2.core.data.rdbs.repository.users;

import com.cqwo.xxx2.core.data.rdbs.repository.BaseRepository;
import com.cqwo.xxx2.core.domain.users.OnlineUserInfo;

public interface OnlineUserRepository extends BaseRepository<OnlineUserInfo, Integer> {
}
