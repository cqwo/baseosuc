package com.cqwo.xxx2.core.data.rdbs.repository.users;

import com.cqwo.xxx2.core.data.rdbs.repository.BaseRepository;
import com.cqwo.xxx2.core.domain.users.UserDetailInfo;

public interface UserDetailRepository extends BaseRepository<UserDetailInfo, Integer> {

}