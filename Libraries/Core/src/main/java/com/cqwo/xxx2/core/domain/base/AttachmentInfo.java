package com.cqwo.xxx2.core.domain.base;

import com.cqwo.xxx2.core.helper.DateHelper;
import com.cqwo.xxx2.core.helper.UnixTimeHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

//附件表
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_attachment")
public class AttachmentInfo implements Serializable {


    /**
     * 附件id
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attachid")
    private Integer attachId = 0;

    /**
     * 用户ID
     */
    @Column(name = "uid", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    private String uid = ""; // 用户ID

    /**
     * 附件地址
     **/
    @Column(name = "url", nullable = false, length = 350)
    @ColumnDefault(value = "''")
    private String url = "";

    /**
     * 标题
     **/
    @Column(name = "title", nullable = false)
    @ColumnDefault(value = "''")
    private String title = "";

    /**
     * 创建时间
     **/
    @Column(name = "createtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer createTime = 0;


    public AttachmentInfo(String uid, String url, String title, Integer createTime) {
        this.uid = uid;
        this.url = url;
        this.title = title;
        this.createTime = createTime;
    }

    public AttachmentInfo(String url, String uid, String title) {
        this.uid = uid;
        this.url = url;
        this.title = title;
        this.createTime = UnixTimeHelper.getUnixTimeStamp();
    }


}