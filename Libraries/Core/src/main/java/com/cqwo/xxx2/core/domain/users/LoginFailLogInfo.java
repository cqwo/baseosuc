/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.xxx2.core.domain.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

//登录日志
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_user_loginfaillogs")
public class LoginFailLogInfo implements Serializable {

    private static final long serialVersionUID = 5100659918606336335L;
    /**
     * 日志Id
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id = 0;

    /**
     * 登录IP
     **/
    @Column(name = "loginip", nullable = false)
    @ColumnDefault(value = "0")
    private long loginIP = 0;

    /**
     * 失败的次数
     **/
    @Column(name = "failtimes", nullable = false)
    @ColumnDefault(value = "0")
    private Integer failTimes = 0;

    /**
     * 最后登陆时间
     **/
    @Column(name = "lastlogintime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer lastLoginTime = 0;


}