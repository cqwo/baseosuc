
package com.cqwo.xxx2.core.data.rdbs.repository.authors;

import com.cqwo.xxx2.core.data.rdbs.repository.BaseRepository;
import com.cqwo.xxx2.core.domain.authors.AuthorSessionInfo;

public interface AuthorSessionRepository extends BaseRepository<AuthorSessionInfo, Integer> {
}