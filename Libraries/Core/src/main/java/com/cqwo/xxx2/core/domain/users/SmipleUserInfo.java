/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.xxx2.core.domain.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 *
 * @author cqnews
 * @date 2017/12/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SmipleUserInfo implements Serializable {

    /**
     * 用户姓名
     */
    private String realName="";

    /**
     * 手机号码
     */
    private String mobile="127.0.0.1";

    /**
     * ip
     */
    private String ip="";


}
