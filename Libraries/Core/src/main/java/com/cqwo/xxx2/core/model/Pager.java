package com.cqwo.xxx2.core.model;

/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 分页
 *
 * @author cqnews
 * @dData2017/12/14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pager implements Serializable {


    protected boolean showSummary = true;//是否显示汇总

    protected boolean showItems = true;//是否显示页项

    protected Integer itemCount = 7;//项数量

    protected boolean showFirst = true;//是否显示首页

    protected boolean showPre = true;//是否显示上一页

    protected boolean showNext = true;//是否显示下一页

    protected boolean showLast = true;//是否显示末页

    protected boolean showPageSize = true;//是否显示每页数

    protected boolean showGoPage = true;//是否显示页数输入框


}
