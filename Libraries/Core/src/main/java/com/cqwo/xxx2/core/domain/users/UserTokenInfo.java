package com.cqwo.xxx2.core.domain.users;

import com.cqwo.xxx2.core.helper.DateHelper;
import com.cqwo.xxx2.core.helper.UnixTimeHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用户token
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_users_token")
public class UserTokenInfo implements Serializable {


    private static final long serialVersionUID = 7223137506659169031L;

    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id = 0;

    /**
     * 要求唯一
     * uid
     */
    @Column(name = "uid", unique = true, nullable = false)
     @ColumnDefault(value = "''")
    private String uid = "";


    /**
     * token
     */
    @Column(name = "token", nullable = false, length = 500)
     @ColumnDefault(value = "''")
    private String token = "";

    /**
     * 过期时间
     */
    @Column(name = "limittime", nullable = false)
    @ColumnDefault("0")
    private Integer limitTime = 0;

    public UserTokenInfo(String uid, String token) {
        this.uid = uid;
        this.token = token;
        this.limitTime = UnixTimeHelper.getUnixTimeStamp() + 60 * 60;
    }

    public UserTokenInfo(int id, String uid, String token) {
        this.id = id;
        this.uid = uid;
        this.token = token;
        this.limitTime = UnixTimeHelper.getUnixTimeStamp() + 60 * 60;

    }
}
