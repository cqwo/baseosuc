package com.cqwo.xxx2.core.ucenter;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component(value = "CWMUCenter")
public class CWMUCenter {

    @Autowired(required = false)
    private IUCenterStrategy iUCenterStrategy;


}
