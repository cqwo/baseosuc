package com.cqwo.xxx2.core.exception;

/**
 * @author cqnews
 */
public class PostSuccessException extends CWMException {

    private static final long serialVersionUID = -6913708326594560011L;

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public PostSuccessException(String message) {
        super(message);
    }
}
