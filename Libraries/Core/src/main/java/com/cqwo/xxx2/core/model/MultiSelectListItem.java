package com.cqwo.xxx2.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 多选择器
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MultiSelectListItem implements Serializable {

    public SelectListItem item;

    public List<SelectListItem> itemList = new ArrayList<>();

    public MultiSelectListItem(SelectListItem item) {
        this.item = item;
    }

}
