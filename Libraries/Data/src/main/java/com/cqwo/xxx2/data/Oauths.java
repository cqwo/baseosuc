package com.cqwo.xxx2.data;

import com.cqwo.ucenter.client.domain.OauthInfo;
import com.cqwo.ucenter.client.exception.UCenterException;
import com.cqwo.xxx2.core.ucenter.CWMUCenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cqnews on 2017/4/11.
 */


//第三方登录
@Service(value = "OauthsData")
public class Oauths extends DataService {

    @Autowired
    private CWMUCenter cwmuCenter;

    public OauthInfo findOauthByUid(String uid) throws UCenterException {
        return cwmuCenter.getIUCenterStrategy().findOauthByUid(uid);
    }


    //endregion

}
