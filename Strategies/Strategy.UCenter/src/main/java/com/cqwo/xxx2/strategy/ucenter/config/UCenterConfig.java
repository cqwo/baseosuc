package com.cqwo.xxx2.strategy.ucenter.config;


import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "user.config")
@PropertySource("classpath:ucenterconfig.properties")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UCenterConfig {


    /**
     * appId
     */
    private String appId = "";

    /**
     * 用户中心app名称
     */
    private String application = "ucenter";

    /**
     * apikey
     */
    private String apiKey = "";


    /**
     * apiSecret
     */
    private String apiSecret = "";


}
