package com.cqwo.xxx2.strategy.ucenter;


import com.cqwo.ucenter.client.impl.UcenterClientImpl;
import com.cqwo.xxx2.core.ucenter.IUCenterStrategy;
import com.cqwo.xxx2.strategy.ucenter.config.UCenterConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component(value = "UCenterStrategy")
public class UCenterStrategy extends UcenterClientImpl implements IUCenterStrategy {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UCenterConfig uCenterConfig;


    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public String getAppId() {
        return uCenterConfig.getAppId();
    }

    @Override
    public String getApiKey() {
        return uCenterConfig.getApiKey();
    }

    @Override
    public String getApiSecret() {
        return uCenterConfig.getApiSecret();
    }


}
