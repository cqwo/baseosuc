# BaseOS

#### 项目介绍
微信开发者项目

#### 软件架构

软件基于JAVA语言开始,采用了springboot整合框架,主要的框架有springmvc,spring,hibernate,redis,shiro框架


#### 安装教程
1.git项目
2.配置数据库地址
3.配置redis地址
4.插入数据库默认数据




#### 使用说明



#### 参与贡献

1. 刘崇科
2. 马吉刚

### 更新记录

1.所有的UI升级,404,错误界面,管理界面.提示页面
2.升级所有的Helper
3.升级上下文版本.web.framewrok
4.升级springboot版本2.1.4




