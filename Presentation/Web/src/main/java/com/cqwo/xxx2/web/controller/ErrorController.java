package com.cqwo.xxx2.web.controller;

import com.cqwo.xxx2.web.framework.controller.BaseWebController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * <pre>
 * 出错页面控制器
 * Created by Binary Wang on 2018/8/25.
 * </pre>
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 */
@RestController
public class ErrorController extends BaseWebController {


    @RequestMapping(value = "/404")
    public ModelAndView err404() {
        return View();
    }


}
