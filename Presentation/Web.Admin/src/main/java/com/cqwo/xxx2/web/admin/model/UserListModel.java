package com.cqwo.xxx2.web.admin.model;

import com.cqwo.ucenter.client.domain.PartUserInfo;
import com.cqwo.xxx2.core.model.PageModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserListModel {


    /**
     * 用户uid
     */
    private String uid = "";

    /**
     * 用户昵称
     */
    private String nickName = "";

    /**
     * 手机号码
     */
    private String mobile = "";

    /**
     * 用户列表
     */
    private List<PartUserInfo> userInfoList;

    /**
     * 分页模型
     */
    private PageModel pageModel;


    public UserListModel(List<PartUserInfo> userInfoList, PageModel pageModel) {
        this.userInfoList = userInfoList;
        this.pageModel = pageModel;
    }

}
