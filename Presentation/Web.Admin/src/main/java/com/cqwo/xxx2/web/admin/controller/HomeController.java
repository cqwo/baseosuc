package com.cqwo.xxx2.web.admin.controller;

import com.cqwo.xxx2.web.framework.controller.BaseAdminController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HomeController extends BaseAdminController {

    @RequestMapping("index")
    public ModelAndView index() {

        return View();
    }


    /**
     * 运行
     *
     * @return
     */
    @RequestMapping("runinfo")
    public ModelAndView runinfo() {

        return View();
    }
}
