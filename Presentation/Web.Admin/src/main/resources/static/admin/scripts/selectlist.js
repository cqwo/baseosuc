﻿/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */


$(function () {
    loadDeviceList(1, "");
});

function loadDeviceList(pageNumber, keyword) {

    var httpUrl = "/admin/lightpole/position/ajaxdevicelist?pageSize=" + 24 + "&pageNumber=" + pageNumber + "&keyword=" + keyword;

    $.getJSON(httpUrl, function (data) {

        console.log(data);
        //


        var message = data.message;
        var state = data.state;

        if (state !== 0) {
            alert("没有更多数据了");
        } else {

            var content = data.content;

            //console.log(content2);

            var deviceItemList = content.deviceItemList;
            //console.log("DeviceItemList:" + DeviceItemList);

            var html5 = "<ul>";

            for (var i = 0; i < deviceItemList.length; i++) {

                html5 += "<li><a onclick=\"setSelectedDevice(this)\" deviceId=\"" + deviceItemList[i].value + "\">" + deviceItemList[i].text + "</a></li>";

            }

            html5 += "<div class=\"clear\"></div>";

            html5 += "</ul>";
            html5 += "<div class=\"clear\"></div>";

            $("#selectDeviceBoxCon").html(html5);

            var totalPages = parseInt(content.totalPages);
            var startPageNumber = 1;
            var endPageNumber = 1;

            if (pageNumber === totalPages && totalPages >= 9) {
                startPageNumber = totalPages - 9;
            } else if (pageNumber - 4 >= 1) {
                startPageNumber = pageNumber - 4;
            } else {
                startPageNumber = 1;
            }
            if (pageNumber === 1 && totalPages >= 9) {
                endPageNumber = 9;
            } else if (pageNumber + 4 <= totalPages) {
                endPageNumber = pageNumber + 4;
            } else {
                endPageNumber = totalPages;
            }

            var pagehtml = "";

            pagehtml += "<a href='javascript:;' class='bt' onclick='goDeviceSelectListPage(this)' pageNumber='1'><<</a>";
            for (var i = startPageNumber; i <= endPageNumber; i++) {
                if (i !== pageNumber) {
                    pagehtml += "<a href='javascript:;' class='bt' onclick='goDeviceSelectListPage(this)' pageNumber='" + i + "'>" + i + "</a>";
                } else {
                    pagehtml += "<a href='javascript:;' class='bt hot'>" + i + "</a>";
                }
            }
            pagehtml += "<a href='javascript:;' class='bt' onclick='goDeviceSelectListPage(this)' pageNumber='" + totalPages + "'>>></a>";

            pagehtml += "</div></td></tr></table></div>";

            $("#selectDevicePage").html(pagehtml);

            return;
        }
        try {
        } catch (e) {
            alert("没有更多数据了");

        }

    }, function (error) {
        alert(error);
    })

}

function setSelectedDevice(item) {

    var deviceId = $(item).attr("deviceId");
    var deivceName = $(item).html();

    $("#bindDeivceIdBtn").val(deivceName);
    $("#bindDeivceId").val(deviceId);
    $("#bindDeivceName").val(deivceName);
    $("#myDeviceModal").modal("hide");
}

function searchDeviceSelectList() {
    var oldSearchDeviceOfSelectList = $("#searchDeviceOfSelectList").val();
    loadDeviceList(1, oldSearchDeviceOfSelectList);
}

function goDeviceSelectListPage(pageObj) {
    var searchDeviceOfSelectList = $("#searchDeviceOfSelectList").val();
    loadDeviceList($(pageObj).attr("pageNumber"), searchDeviceOfSelectList);
}