package com.cqwo.xxx2.web.framework.shiro;


import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import com.cqwo.xxx2.web.framework.filter.ApiAccessControlFilter;
import com.cqwo.xxx2.web.framework.shiro.admin.AdminRealm;
import com.cqwo.xxx2.web.framework.shiro.api.ApiRealm;
import com.cqwo.xxx2.web.framework.shiro.token.TokenRealm;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.mgt.SessionsSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.Filter;
import java.util.List;
import java.util.Map;

@Configuration
public class ShiroConfig {


    private static final Logger logger = LoggerFactory.getLogger(ShiroConfig.class);


    @Bean
    protected SessionsSecurityManager securityManager() {

        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

        securityManager.setAuthenticator(modularRealmAuthenticator());
        List<Realm> realms = Lists.newArrayList();
        realms.add(adminRealm());
        realms.add(apiRealm());
        realms.add(tokenRealm());

        securityManager.setRealms(realms);

        return securityManager;
    }


    /**
     * 系统自带的Realm管理，主要针对多realm
     */
    @Bean
    public ModularRealmAuthenticator modularRealmAuthenticator() {
        //自己重写的ModularRealmAuthenticator
        UserModularRealmAuthenticator modularRealmAuthenticator = new UserModularRealmAuthenticator();
        modularRealmAuthenticator.setAuthenticationStrategy(new AtLeastOneSuccessfulStrategy());
        return modularRealmAuthenticator;
    }

    @Bean
    public AccessControlFilter myAccessControlFilter() {

        return new ApiAccessControlFilter();
    }


    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) { //System.out.println("ShiroConfiguration.shirFilter()");

        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();

        // 必须设置 SecurityManager
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面
        shiroFilterFactoryBean.setLoginUrl("/login");


        // 登录成功后要跳转的链接
        shiroFilterFactoryBean.setSuccessUrl("/index");
        //未授权界面;
        shiroFilterFactoryBean.setUnauthorizedUrl("/403");


        //自定义拦截器
        Map<String, Filter> filtersMap = Maps.newLinkedHashMap();
        filtersMap.put("apiAccessControlFilter", new ApiAccessControlFilter());
        shiroFilterFactoryBean.setFilters(filtersMap);

        //拦截器.
        Map<String, String> filterChainDefinitionMap = Maps.newLinkedHashMap();

        //filterChainDefinitionMap.put("/api/author/**", "anon");
        filterChainDefinitionMap.put("/api/login", "anon");
        filterChainDefinitionMap.put("/api/**", "apiAccessControlFilter");
        filterChainDefinitionMap.put("/sapi/**", "anon");
        filterChainDefinitionMap.put("/gsapi/**", "anon");
        filterChainDefinitionMap.put("/admin/tool/test", "anon");


        filterChainDefinitionMap.put("/static/**", "anon"); //匿名访问静态资源
        filterChainDefinitionMap.put("/components/**", "anon"); //匿名访问静态资源
        filterChainDefinitionMap.put("/admin/tool/device/list", "anon");


        filterChainDefinitionMap.put("/admin/**", "authc");
        filterChainDefinitionMap.put("/service/admin/**", "authc");
        filterChainDefinitionMap.put("/street/**", "authc");


        filterChainDefinitionMap.put("/", "anon");

        filterChainDefinitionMap.put("/token/login", "anon");
        filterChainDefinitionMap.put("/token/**", "authc");


        //配置退出过滤器,其中的具体的退出代码Shiro已经替我们实现了
        filterChainDefinitionMap.put("/logout", "logout");

        filterChainDefinitionMap.put("/*.txt", "anon");

        filterChainDefinitionMap.put("/tool/**", "anon"); //匿名访问静态资源

        filterChainDefinitionMap.put("/**", "anon");//authc

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }


    @Bean
    public AdminRealm adminRealm() {

        AdminRealm userRealm = new AdminRealm();
        userRealm.setCachingEnabled(true);
        return userRealm;

    }


    @Bean
    public ApiRealm apiRealm() {

        ApiRealm apiRealm = new ApiRealm();
        apiRealm.setCachingEnabled(true);
        return apiRealm;

    }


    @Bean
    public TokenRealm tokenRealm() {

        TokenRealm userRealm = new TokenRealm();
        userRealm.setCachingEnabled(true);
        return userRealm;

    }

    @Bean
    public ShiroDialect shiroDialect() {
        return new ShiroDialect();
    }


//    @Bean
//    public Realm realm() {
//
//        APIRealm apiRealm = new APIRealm();
//        apiRealm.setCachingEnabled(true);
//
//        return apiRealm;
//    }

//    @Override
//    protected ShiroFilterFactoryBean shiroFilterFactoryBean() {
//        ShiroFilterFactoryBean factoryBean = super.shiroFilterFactoryBean();
////        Map<String, Filter> filterMap = new LinkedHashMap<>();
////        //添加自定义的Filter,这里我随便new了一个filter
////        filterMap.put("anyrole", new MyAccessControlFilter());
////        factoryBean.setFilters(filterMap);
//        return factoryBean;
//    }


    @Bean
    protected CacheManager cacheManager() {
        return new MemoryConstrainedCacheManager();
    }

    @ModelAttribute(name = "subject")
    public Subject subject() {
        return SecurityUtils.getSubject();
    }

    /**
     * Shiro生命周期处理器 * @return
     */
    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    /**
     * 开启Shiro的注解(如@RequiresRoles,@RequiresPermissions),需借助SpringAOP扫描使用Shiro注解的类,并在必要时进行安全逻辑验证 * 配置以下两个bean(DefaultAdvisorAutoProxyCreator(可选)和AuthorizationAttributeSourceAdvisor)即可实现此功能 * @return
     */
    @Bean
    @DependsOn({"lifecycleBeanPostProcessor"})
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }


}