package com.cqwo.xxx2.web.framework.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author cqnews
 */
@Component
@ConfigurationProperties(prefix = "com.cqwo.xxx2.web.api.config")
@PropertySource("classpath:webapiconfig.properties")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WebApiConfig implements Serializable {

    private static final long serialVersionUID = 6337405173921270430L;


    /**
     * 产品编号
     */
    private String apiKey = "";
    /**
     * 产品编号
     */
    private String apiSecret = "";

}
