/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.xxx2.web.framework;

import com.cqwo.xxx2.core.helper.WebHelper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 视图模型
 *
 * @author cqnews
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PromptModel implements Serializable {

    private static final long serialVersionUID = -6848647052535810843L;

    @Builder.Default
    private Integer error = 0;

    /**
     * 提示信息
     */
    @Builder.Default
    private String message = "";

    /**
     * 返回地址
     */
    @Builder.Default
    private String backUrl = "";

    /**
     * refresh地址
     */
    @Builder.Default
    private String refreshContent = "";


    /**
     * 倒计时模型
     */
    @Builder.Default
    private int countDownModel = 0;

    /**
     * 倒计时时间
     */
    @Builder.Default
    private int countDownTime = 2;

    /**
     * 是否显示返回地址
     */
    @Builder.Default
    private boolean showBackLink = true;

    /**
     * 是否自动返回
     */
    @Builder.Default
    private boolean autoBack = true;


    /**
     * 模型构造函数
     *
     * @param message 消息
     */
    public PromptModel(String message) {
        this.message = message;
    }

    /**
     * 模型构造函数
     *
     * @param message 消息
     * @param backUrl 返回地址
     */
    public PromptModel(String message, String backUrl) {
        this.backUrl = backUrl; //System.out.println("backUrl:" + WebHelper.urlDecode(this.backUrl));
        this.refreshContent = countDownTime + ";URL=" + backUrl;
        this.autoBack = true;
        this.showBackLink =true;
        this.message = message;
    }

    /**
     * 模型构造函数
     *
     * @param error   错误类型
     * @param message 消息
     * @param backUrl 返回地址
     */
    public PromptModel(Integer error, String message, String backUrl) {
        this.error = error;
        this.message = message;
        this.backUrl = backUrl;
        this.autoBack = true;
        this.showBackLink =true;
        this.refreshContent = countDownTime + ";URL=" + backUrl;
    }

    /**
     * 模型构造函数
     *
     * @param backUrl  返回地址
     * @param message  消息
     * @param autoBack 是否自动返回
     */
    public PromptModel(String message, String backUrl, boolean autoBack) {
        this.message = message;
        this.backUrl = backUrl;
        this.autoBack = autoBack;
        this.showBackLink =true;
        this.refreshContent = countDownTime + ";URL=" + backUrl;
    }

    /**
     * 模型构造函数
     *
     * @param error    错误类型
     * @param message  消息
     * @param backUrl  返回地址
     * @param autoBack 是否自动返回
     */
    public PromptModel(Integer error, String message, String backUrl, boolean autoBack) {
        this.error = error;
        this.message = message;
        this.backUrl = backUrl;
        this.showBackLink =true;
        this.refreshContent = countDownTime + ";URL=" + backUrl;
        this.autoBack = autoBack;
    }

    /**
     * 模型构造函数
     *
     * @param message       消息
     * @param backUrl       返回地址
     * @param countDownTime 倒计时时间
     */
    public PromptModel(String message, String backUrl, int countDownTime) {
        this.message = message;
        this.backUrl = backUrl;
        this.refreshContent = countDownTime + ";URL=" + backUrl;
        this.autoBack = true;
        this.showBackLink =true;
        this.countDownTime = countDownTime;
    }


    /**
     * 模型构造函数
     *
     * @param error         错误类型
     * @param message       消息
     * @param backUrl       返回地址
     * @param countDownTime 倒计时时间
     */
    public PromptModel(Integer error, String message, String backUrl, int countDownTime) {
        this.error = error;
        this.message = message;
        this.backUrl = backUrl;
        this.autoBack = true;
        this.showBackLink =true;
        this.refreshContent = countDownTime + ";URL=" + backUrl;
        this.countDownTime = countDownTime;
    }

    /**
     * 模型构造函数
     *
     * @param backUrl        返回地址
     * @param message        消息
     * @param countDownModel 计数模型
     * @param countDownModel 倒计时时间
     * @param showBackLink   是否显示返回地址
     * @param autoBack       是否自动返回
     */
    public PromptModel(String message, String backUrl, int countDownModel, int countDownTime, boolean showBackLink, boolean autoBack) {
        this.message = message;
        this.backUrl = backUrl;
        this.refreshContent = countDownTime + ";URL=" + backUrl;
        this.countDownModel = countDownModel;
        this.countDownTime = countDownTime;
        this.showBackLink = showBackLink;
        this.autoBack = autoBack;
    }


    /**
     * 模型构造函数
     *
     * @param error          错误类型
     * @param message        消息
     * @param backUrl        返回地址
     * @param countDownModel 倒计时时间
     */
    public PromptModel(Integer error, String message, String backUrl, int countDownModel, int countDownTime, boolean showBackLink, boolean autoBack) {
        this.error = error;
        this.message = message;
        this.backUrl = backUrl;
        this.refreshContent = countDownTime + ";URL=" + backUrl;
        this.countDownModel = countDownModel;
        this.countDownTime = countDownTime;
        this.showBackLink = showBackLink;
        this.autoBack = autoBack;
    }

}
