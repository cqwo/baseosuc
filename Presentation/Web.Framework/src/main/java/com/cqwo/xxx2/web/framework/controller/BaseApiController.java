/*
 *  *  Copyright  2019.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2019.  510Link.com Iniot All rights reserved.
 */

package com.cqwo.xxx2.web.framework.controller;

import com.cqwo.ucenter.client.domain.PartUserInfo;
import com.cqwo.xxx2.core.domain.authors.AuthUserInfo;
import com.cqwo.xxx2.core.exception.CWMAPIException;
import com.cqwo.xxx2.core.exception.CWMException;
import com.cqwo.xxx2.core.helper.WebHelper;
import com.cqwo.xxx2.services.UserRanks;
import com.cqwo.xxx2.services.Users;
import com.cqwo.xxx2.web.framework.config.WebApiConfig;
import com.cqwo.xxx2.web.framework.workcontext.AuthorApiWorkContext;
import com.google.common.base.Strings;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping(value = "api")
public class BaseApiController extends BaseController {

    protected AuthorApiWorkContext workContext;


    @Autowired
    protected Users users;

    @Autowired
    protected UserRanks userRanks;

    @Autowired
    protected WebApiConfig webApiConfig;


    public BaseApiController() {

    }

    @Override
    AuthorApiWorkContext getWorkContext() {
        return workContext;
    }

    @ModelAttribute
    public void setInitialize(HttpServletResponse response) throws CWMException, IOException {

        this.response = response;
        this.workContext = new AuthorApiWorkContext();
        this.session = request.getSession();

        /*
         * 获取当前URL
         */
        this.workContext.setUrl(WebHelper.getUrl(request));

        /*
         * 获取当前controller
         */
        this.workContext.setController(this.getClass().getName());

        /*
         * 获取当前action 暂时不能实现
         */
        this.workContext.setAction(WebHelper.getRawUrl(request));


        /*
         * 判断是否为ajax
         */
        if (WebHelper.isAjax(request)) {
            this.workContext.setHttpAjax(true);
        }

        /*
         * 获取IP
         */
        this.workContext.setIP(WebHelper.getIP(request));


        this.workContext.setUrlReferrer(WebHelper.getUrlReferrer(request));


        try {


            String apiKey = cwmUtils.getApiKeyHeader();

            if (!webApiConfig.getApiKey().equals(apiKey)) {
                throw new CWMAPIException("ApiKey异常");
            }

            String apiSecret = cwmUtils.getApiSecretHeader();

            if (!webApiConfig.getApiSecret().equals(apiSecret)) {
                throw new CWMAPIException("ApiSecret异常");
            }

            AuthUserInfo authInfo = (AuthUserInfo) SecurityUtils.getSubject().getPrincipal();

            if (authInfo == null) {
                throw new CWMAPIException("用户登录异常");
            }

            PartUserInfo userInfo = authInfo.getUserInfo();

            if (userInfo == null || Strings.isNullOrEmpty(userInfo.getUid())) {
                throw new CWMAPIException("用户信息异常");
            }

            workContext.setUid(authInfo.getUid());
            workContext.setUsername(authInfo.getUsername());
            workContext.setMobile(userInfo.getMobile());
            workContext.setEmail(userInfo.getEmail());
            workContext.setUserInfo(userInfo);


        } catch (Exception ex) {

            logs.write(ex, "上午文处理报错");

            throw new CWMAPIException("异常退出");
        }


        this.workContext.setImageCDN("/static/admin/images");
        this.workContext.setCssCDN("/static/admin/css");
        this.workContext.setScriptCDN("/static/admin/scripts");
        this.workContext.setFontCDN("/static/admin/fonts");


        //Log.e("URL",request.getRequestURI());
    }


    @ModelAttribute
    public void inspectInitialize() {
    }


}
