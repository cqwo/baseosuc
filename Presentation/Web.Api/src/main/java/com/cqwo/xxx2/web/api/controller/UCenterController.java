package com.cqwo.xxx2.web.api.controller;


import com.cqwo.xxx2.web.api.model.UCenterHomeModel;
import com.cqwo.xxx2.web.framework.controller.BaseApiController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.cqwo.xxx2.core.errors.SateCollect.SUCCESS;


/**
 * 用户中心
 */
@RestController(value = "ApiUCenterController")
public class UCenterController extends BaseApiController {


    private Lock lock = new ReentrantLock();

    /**
     * Ucenter用户中心首页
     *
     * @return
     */
    @RequestMapping(value = "ucenter/index")
    public String index() {

        return JsonView(SUCCESS, "用户中心首页");

    }

}
