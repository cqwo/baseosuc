package com.cqwo.xxx2.web.api.model;


import com.cqwo.ucenter.client.domain.PartUserInfo;
import com.cqwo.ucenter.client.domain.UserRankInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户中心首页
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UCenterHomeModel {

    /**
     * 用户uid
     */
    private String uid = "";

    /**
     * 用户信息
     */
    private PartUserInfo userInfo;


    /**
     * 用户分组
     */
    private UserRankInfo userRankInfo;


}
