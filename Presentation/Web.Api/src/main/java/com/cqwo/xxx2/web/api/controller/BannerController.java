package com.cqwo.xxx2.web.api.controller;


import com.cqwo.xxx2.web.framework.controller.BaseApiController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static com.cqwo.xxx2.core.errors.SateCollect.SUCCESS;

/**
 * banner
 */
@RestController(value = "ApiBannerController")
public class BannerController extends BaseApiController {



    @RequestMapping(value = "banner/list")
    public String list(){

        List<String> bannerList = new ArrayList<>();

        return JsonView(SUCCESS, bannerList, "banner加载成功");

    }
}

